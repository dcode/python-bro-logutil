Name:		python-bro-logutil
Version:	0.0.1
Release:	1%{?dist}
Summary:	Python module to manipulate logs from Bro 2.x

Group:		Development/Python
License:	BSD
URL:		
Source0:	

BuildRequires:	
Requires:	

%description


%prep
%setup -q


%build
%configure
make %{?_smp_mflags}


%install
make install DESTDIR=%{buildroot}


%files
%doc



%changelog

